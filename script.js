const images = document.querySelectorAll('.image');

function onTimer() {
    const timeStamp = new Date().getTime();
    images.forEach(image => image.src = `${image.src}?ts=${timeStamp}`);
}
setInterval(onTimer, 10000);
